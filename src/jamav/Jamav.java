package jamav;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import static jamav.ViewFactory.*;

/**
 *
 * @author pavelpopov
 */
public class Jamav extends Application {

    StackPane root;
    Pane contentPane;
    Pane buttonPane;
    EventHandler<ActionEvent> backHandler;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        root = new StackPane();
        root.setPadding(new Insets(25));
        buttonPane = new VBox(10);

        backHandler = (ActionEvent event) -> {
            root.getChildren().clear();
            root.getChildren().add(buttonPane);
        };

        Button bubbleSortBtn = new Button();
        bubbleSortBtn.setText("Сортировка пузырьком");
        bubbleSortBtn.setOnAction((ActionEvent event) -> {
            root.getChildren().clear();
            EditableGrid editableGrid = new EditableGrid(true);
            contentPane = createContentPane(editableGrid, createStepButtons(backHandler, (ActionEvent e) -> {
                runAlgorithm(Algorithms.BubbleSort, editableGrid);
            }));
            root.getChildren().add(contentPane);
        });
        buttonPane.getChildren().add(bubbleSortBtn);

        Button insertionSortBtn = new Button();
        insertionSortBtn.setText("Сортировка вставками");
        insertionSortBtn.setOnAction((ActionEvent event) -> {
            root.getChildren().clear();
            EditableGrid editableGrid = new EditableGrid(true);
            contentPane = createContentPane(editableGrid, createStepButtons(backHandler, (ActionEvent e) -> {
                runAlgorithm(Algorithms.InsertionSort, editableGrid);
            }));
            root.getChildren().add(contentPane);
        });
        buttonPane.getChildren().add(insertionSortBtn);

        Button selectionSortBtn = new Button();
        selectionSortBtn.setText("Сортировка выбором");
        selectionSortBtn.setOnAction((ActionEvent event) -> {
            root.getChildren().clear();
            EditableGrid editableGrid = new EditableGrid(true);
            contentPane = createContentPane(editableGrid, createStepButtons(backHandler, (ActionEvent e) -> {
                runAlgorithm(Algorithms.SelectionSort, editableGrid);
            }));
            root.getChildren().add(contentPane);
        });
        buttonPane.getChildren().add(selectionSortBtn);

        Button pathFinding = new Button();
        pathFinding.setText("Поиск пути");
        pathFinding.setOnAction((ActionEvent event) -> {
            root.getChildren().clear();
            EditableGrid editableGrid = new EditableGrid(false);
            contentPane = createContentPane(editableGrid, createStepButtons(backHandler, (ActionEvent e) -> {
                runAlgorithm(Algorithms.PathFinding, editableGrid);
            }));
            root.getChildren().add(contentPane);
        });
        buttonPane.getChildren().add(pathFinding);

        root.getChildren().add(buttonPane);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Алгоритмы");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void runAlgorithm(Algorithms type, EditableGrid editableGrid) {
        root.getChildren().clear();
        DisplayGrid displayGrid = new DisplayGrid(editableGrid.getValue());
        contentPane = createContentPane(displayGrid, createDemoButtons((ActionEvent e) -> {
            runAlgorithm(type, editableGrid);
        }, backHandler));
        root.getChildren().add(contentPane);
        runAlgorithm(type, displayGrid);
    }

    public void runAlgorithm(Algorithms type, DisplayGrid grid) {
        Thread t = new Thread(() -> {
            type.getImpl().run(grid);
        }, type.toString() + " Thread");
        t.setDaemon(true);
        t.start();
    }

    public static void sleep() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
        }
    }

}
