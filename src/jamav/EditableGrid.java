package jamav;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import static jamav.GridUtil.*;
import static jamav.ViewFactory.*;

/**
 *
 * @author pavelpopov
 */
public class EditableGrid extends GridPane {

    private boolean isArray;
    private GridPane grid;

    public EditableGrid(boolean isArray) {
        this.isArray = isArray;
        if (isArray) {
            grid = createInputGrid();
            Button xExt = createInputExt((ActionEvent event) -> {
                int cols = getColumnCount(grid);
                grid.add(createInputField(), cols, 0);
            });
            Button xDel = createInputDel((ActionEvent event) -> {
                int cols = getColumnCount(grid);
                if (cols > 1) {
                    Node n = getNodeByRowColumnIndex(grid, cols - 1, 0);
                    grid.getChildren().remove(n);
                }
            });
            add(grid, 0, 0);
            add(xExt, 1, 0);
            add(xDel, 2, 0);
        } else {
            grid = createInputGrid();
            Button xExt = createInputExt(null);
            Button yExt = createInputExt(null);
            Button xDel = createInputDel(null);
            Button yDel = createInputDel(null);
            xExt.setOnAction((ActionEvent event) -> {
                int cols = getColumnCount(grid);
                int rows = getRowCount(grid);
                for (int i = 0; i < rows; i++) {
                    grid.add(createInputField(), cols, i);
                }
                xExt.setMinHeight(rows * 45);
                xDel.setMinHeight(rows * 45);
                yExt.setMinWidth((cols + 1) * 45);
                yDel.setMinWidth((cols + 1) * 45);
            });
            yExt.setOnAction((ActionEvent event) -> {
                int cols = getColumnCount(grid);
                int rows = getRowCount(grid);
                for (int i = 0; i < cols; i++) {
                    grid.add(createInputField(), i, rows);
                }
                xExt.setMinHeight((rows + 1) * 45);
                xDel.setMinHeight((rows + 1) * 45);
                yExt.setMinWidth(cols * 45);
                yDel.setMinWidth(cols * 45);
            });
            xDel.setOnAction((ActionEvent event) -> {
                int cols = getColumnCount(grid);
                int rows = getRowCount(grid);
                if (cols > 1) {
                    for (int i = 0; i < rows; i++) {
                        Node n = getNodeByRowColumnIndex(grid, cols - 1, i);
                        grid.getChildren().remove(n);
                    }
                }
                xExt.setMinHeight(rows * 45);
                xDel.setMinHeight(rows * 45);
                yExt.setMinWidth((cols - 1) * 45);
                yDel.setMinWidth((cols - 1) * 45);
            });
            yDel.setOnAction((ActionEvent event) -> {
                int cols = getColumnCount(grid);
                int rows = getRowCount(grid);
                if (rows > 1) {
                    for (int i = 0; i < cols; i++) {
                        Node n = getNodeByRowColumnIndex(grid, i, rows - 1);
                        grid.getChildren().remove(n);
                    }
                }
                xExt.setMinHeight((rows - 1) * 45);
                xDel.setMinHeight((rows - 1) * 45);
                yExt.setMinWidth(cols * 45);
                yDel.setMinWidth(cols * 45);
            });
            add(grid, 0, 0);
            add(xExt, 1, 0);
            add(yExt, 0, 1);
            add(xDel, 2, 0);
            add(yDel, 0, 2);
        }
    }

    public boolean isArray() {
        return isArray;
    }

    public int[][] getValue() {
        return GridUtil.getValue(grid);
    }
}
