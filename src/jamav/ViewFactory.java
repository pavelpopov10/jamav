package jamav;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.*;

/**
 *
 * @author pavelpopov
 */
public class ViewFactory {

    public static NumberField createInputField() {
        NumberField field = new NumberField();
        field.setMinSize(45.0, 45.0);
        field.setMaxSize(45.0, 45.0);
        return field;
    }

    public static GridPane createInputGrid() {
        GridPane pane = new GridPane();
        pane.add(createInputField(), 0, 0);
        return pane;
    }

    public static Button createInputExt(EventHandler<ActionEvent> handler) {
        Button extButton = new Button();
        extButton.setMinSize(45.0, 45.0);
        extButton.setText("+");
        extButton.setOnAction(handler);
        return extButton;
    }

    public static Button createInputDel(EventHandler<ActionEvent> handler) {
        Button extButton = new Button();
        extButton.setMinSize(45.0, 45.0);
        extButton.setText("-");
        extButton.setOnAction(handler);
        return extButton;
    }

    public static HBox createStepButtons(EventHandler<ActionEvent> backHandler, EventHandler<ActionEvent> nextHandler) {
        HBox pane = new HBox();
        Button backBtn = new Button();
        backBtn.setMinSize(90.0, 15.0);
        backBtn.setText("Назад");
        backBtn.setOnAction(backHandler);
        pane.getChildren().add(backBtn);
        Button nextBtn = new Button();
        nextBtn.setMinSize(90.0, 15.0);
        nextBtn.setText("Вперёд");
        nextBtn.setOnAction(nextHandler);
        pane.getChildren().add(nextBtn);
        return pane;
    }

    public static HBox createDemoButtons(EventHandler<ActionEvent> restartHandler, EventHandler<ActionEvent> finishHandler) {
        HBox pane = new HBox();
        Button restartBtn = new Button();
        restartBtn.setMinSize(90.0, 15.0);
        restartBtn.setText("Повторить");
        restartBtn.setOnAction(restartHandler);
        pane.getChildren().add(restartBtn);
        Button finishBtn = new Button();
        finishBtn.setMinSize(90.0, 15.0);
        finishBtn.setText("Завершить");
        finishBtn.setOnAction(finishHandler);
        pane.getChildren().add(finishBtn);
        return pane;
    }

    public static VBox createContentPane(Pane content, Pane footer) {
        VBox pane = new VBox(10);
        pane.getChildren().add(content);
        pane.getChildren().add(footer);
        return pane;
    }
}
