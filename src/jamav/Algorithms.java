package jamav;

import jamav.algorithms.PathFindingImpl;
import jamav.algorithms.SelectionSortImpl;
import jamav.algorithms.BubbleSortImpl;
import jamav.algorithms.InsertionSortImpl;

/**
 *
 * @author pavelpopov
 */
public enum Algorithms {
    BubbleSort(new BubbleSortImpl()),
    InsertionSort(new InsertionSortImpl()),
    SelectionSort(new SelectionSortImpl()),
    PathFinding(new PathFindingImpl());

    private final Algorithm impl;

    private Algorithms(Algorithm impl) {
        this.impl = impl;
    }

    public Algorithm getImpl() {
        return impl;
    }
}
