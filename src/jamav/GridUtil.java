package jamav;

import javafx.scene.Node;
import javafx.scene.layout.GridPane;

/**
 *
 * @author pavelpopov
 */
public class GridUtil {

    public static int getColumnCount(GridPane pane) {
        int numCols = pane.getColumnConstraints().size();
        for (int i = 0; i < pane.getChildren().size(); i++) {
            Node child = pane.getChildren().get(i);
            if (child.isManaged()) {
                Integer colIndex = GridPane.getColumnIndex(child);
                if (colIndex != null) {
                    numCols = Math.max(numCols, colIndex + 1);
                }
            }
        }
        return numCols;
    }

    public static int getRowCount(GridPane pane) {
        int numRows = pane.getRowConstraints().size();
        for (int i = 0; i < pane.getChildren().size(); i++) {
            Node child = pane.getChildren().get(i);
            if (child.isManaged()) {
                Integer rowIndex = GridPane.getRowIndex(child);
                if (rowIndex != null) {
                    numRows = Math.max(numRows, rowIndex + 1);
                }
            }
        }
        return numRows;
    }

    public static Node getNodeByRowColumnIndex(GridPane pane, int column, int row) {
        for (int i = 0; i < pane.getChildren().size(); i++) {
            Node child = pane.getChildren().get(i);
            if (child.isManaged()) {
                if (GridPane.getColumnIndex(child) == column && GridPane.getRowIndex(child) == row) {
                    return child;
                }
            }
        }
        return null;
    }

    public static int[][] getValue(GridPane pane) {
        int[][] value = new int[getColumnCount(pane)][getRowCount(pane)];
        for (int i = 0; i < pane.getChildren().size(); i++) {
            Node child = pane.getChildren().get(i);
            if (child.isManaged()) {
                value[GridPane.getColumnIndex(child)][GridPane.getRowIndex(child)] = ((NumberField) child).getNumber();
            }
        }
        return value;
    }
}
