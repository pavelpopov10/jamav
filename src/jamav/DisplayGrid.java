package jamav;

import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author pavelpopov
 */
public class DisplayGrid extends GridPane {

    private int[][] values;
    private State[][] states;
    private static final Font font = new NumberField().getFont();

    public enum State {
        Default, Active, Stored, Red, Green, Yellow, Blue
    }

    public DisplayGrid(int[][] values) {
        this.values = values;
        resetStates();
        redraw();
    }

    public int getXLength() {
        return values.length;
    }

    public int getYLength() {
        return values[0].length;
    }

    public int[] getColumn(int i) {
        return values[i];
    }

    public int[] getRow(int i) {
        int[] row = new int[values.length];
        for (int col = 0; col < values.length; col++) {
            row[col] = values[col][i];
        }
        return row;
    }

    public int getValue(int x, int y) {
        return values[x][y];
    }

    public void setValue(int x, int y, int value) {
        values[x][y] = value;
        redraw();
    }

    public State getState(int x, int y) {
        return states[x][y];
    }

    public void setState(int x, int y, State state) {
        states[x][y] = state;
        redraw();
    }

    public void set(int x, int y, int value, State state) {
        values[x][y] = value;
        states[x][y] = state;
        redraw();
    }

    public void resetStates() {
        states = new State[values.length][values[0].length];
        for (int col = 0; col < values.length; col++) {
            for (int row = 0; row < values[0].length; row++) {
                states[col][row] = State.Default;
            }
        }
    }

    public void redraw() {
        Platform.runLater(() -> {
            getChildren().clear();
            for (int col = 0; col < values.length; col++) {
                for (int row = 0; row < values[0].length; row++) {
                    Color border = Color.BLACK;
                    Color background = Color.WHITE;
                    Color foreground = Color.BLACK;
                    switch (states[col][row]) {
                        case Active:
                            border = Color.RED;
                            background = Color.LIGHTGREY;
                            break;
                        case Stored:
                            background = Color.LIGHTGREY;
                            break;
                        case Red:
                            background = Color.LIGHTCORAL;
                            break;
                        case Green:
                            background = Color.LIGHTGREEN;
                            break;
                        case Yellow:
                            background = Color.LIGHTYELLOW;
                            break;
                        case Blue:
                            background = Color.LIGHTBLUE;
                            break;
                    }
                    Canvas canvas = new Canvas(45, 45);
                    GraphicsContext gc = canvas.getGraphicsContext2D();
                    gc.setFill(border);
                    gc.fillRect(0, 0, 45, 45);
                    gc.setFill(background);
                    gc.fillRect(1, 1, 43, 43);
                    gc.setFill(foreground);
                    gc.setFont(font);
                    String val = String.valueOf(values[col][row]);
                    gc.fillText(val, 10 + (3 - val.length()) * 4, 28);
                    add(canvas, col, row);
                }
            }
        });
    }
}
