package jamav.algorithms;

import jamav.Algorithm;
import jamav.DisplayGrid;
import jamav.DisplayGrid.State;
import static jamav.Jamav.sleep;

/**
 *
 * @author pavelpopov
 */
public class BubbleSortImpl implements Algorithm {

    @Override
    public void run(DisplayGrid grid) {
        int[] arr = grid.getRow(0);
        boolean swapped = true;
        int sorted = 0;
        while (swapped) {
            swapped = false;
            // States reset
            for (int i = 1; i < arr.length - sorted; i++) {
                grid.setState(i, 0, State.Default);
            }
            for (int i = 1; i < arr.length - sorted; i++) {
                grid.setState(i - 1, 0, State.Active);
                grid.setState(i, 0, State.Active);
                sleep();
                // Check
                if (arr[i - 1] > arr[i]) {
                    // Swap
                    int tmp = arr[i - 1];
                    arr[i - 1] = arr[i];
                    arr[i] = tmp;
                    swapped = true;
                    grid.setValue(i - 1, 0, arr[i - 1]);
                    grid.setValue(i, 0, arr[i]);
                }
                grid.setState(i - 1, 0, State.Yellow);
            }
            grid.setState(arr.length - sorted - 1, 0, State.Green);
            sorted++;
            // Changing states of first elements if sorting is finished
            if (!swapped) {
                for (int i = 0; i < arr.length - sorted; i++) {
                    grid.setState(i, 0, State.Green);
                }
            }
        }
    }

}
