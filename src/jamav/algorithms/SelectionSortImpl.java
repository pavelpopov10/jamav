package jamav.algorithms;

import jamav.Algorithm;
import jamav.DisplayGrid;
import jamav.DisplayGrid.State;
import static jamav.Jamav.sleep;

/**
 *
 * @author pavelpopov
 */
public class SelectionSortImpl implements Algorithm {

    @Override
    public void run(DisplayGrid grid) {
        int[] arr = grid.getRow(0);
        for (int i = 0; i < arr.length - 1; i++) {
            int k = i;
            for (int j = i + 1; j < arr.length; j++) {
                grid.setState(k, 0, State.Blue);
                grid.setState(j, 0, State.Active);
                sleep();
                if (arr[j] < arr[k]) {
                    grid.setState(k, 0, State.Yellow);
                    k = j;
                } else {
                    grid.setState(j, 0, State.Yellow);
                }
            }
            int tmp = arr[i];
            arr[i] = arr[k];
            arr[k] = tmp;
            grid.set(k, 0, arr[k], State.Yellow);
            grid.set(i, 0, arr[i], State.Green);
            sleep();
        }
        grid.setState(arr.length - 1, 0, State.Green);
    }

}
