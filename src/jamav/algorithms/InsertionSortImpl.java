package jamav.algorithms;

import jamav.Algorithm;
import jamav.DisplayGrid;
import jamav.DisplayGrid.State;
import static jamav.Jamav.sleep;

/**
 *
 * @author pavelpopov
 */
public class InsertionSortImpl implements Algorithm {

    @Override
    public void run(DisplayGrid grid) {
        int[] arr = grid.getRow(0);
        grid.setState(0, 0, State.Yellow);
        for (int i = 1; i < arr.length; i++) {
            int j = i;
            for (; j > 0 && arr[j - 1] > arr[j]; j--) {
                grid.setState(j, 0, State.Blue);
                grid.setState(j - 1, 0, State.Active);
                sleep();
                int tmp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = tmp;
                grid.set(j, 0, arr[j], State.Yellow);
                grid.setValue(j - 1, 0, arr[j - 1]);
            }
            grid.setState(j, 0, State.Yellow);
        }
        // Changing states of elements when sorting is finished
        for (int i = 0; i < arr.length; i++) {
            grid.setState(i, 0, State.Green);
        }
    }

}
