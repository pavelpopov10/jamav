package jamav.algorithms;

import jamav.Algorithm;
import jamav.DisplayGrid;
import jamav.DisplayGrid.State;
import static jamav.Jamav.sleep;

/**
 *
 * @author pavelpopov
 */
public class PathFindingImpl implements Algorithm {

    @Override
    public void run(DisplayGrid grid) {
        /**
         * Parsing map
         * 0 - empty space
         * 1 - wall
         * 2 - source
         * 3 - target
         */
        int sx = -1, sy = -1;
        int tx = -1, ty = -1;
        for (int x = 0; x < grid.getXLength(); x++) {
            for (int y = 0; y < grid.getYLength(); y++) {
                if (grid.getValue(x, y) == 1) {
                    grid.setState(x, y, State.Red);
                }
                if (sx < 0 && sy < 0 && grid.getValue(x, y) == 2) {
                    sx = x;
                    sy = y;
                    grid.setState(x, y, State.Stored);
                } else if (tx < 0 && ty < 0 && grid.getValue(x, y) == 3) {
                    tx = x;
                    ty = y;
                    grid.setState(x, y, State.Blue);
                }
            }
        }
        if (sx < 0 || tx < 0) {
            System.err.println("Source or target was not found!");
            return;
        }
        // Initializing wave field
        int[][] marks = new int[grid.getXLength()][grid.getYLength()];
        boolean[][] walls = new boolean[grid.getXLength()][grid.getYLength()];
        for (int x = 0; x < grid.getXLength(); x++) {
            for (int y = 0; y < grid.getYLength(); y++) {
                walls[x][y] = grid.getValue(x, y) == 1;
                marks[x][y] = -1;
                grid.setValue(x, y, -1);
            }
        }
        // Setting up first cell
        marks[sx][sy] = 0;
        grid.setValue(sx, sy, 0);
        sleep();
        // Sending waves
        for (int d = 0; marks[tx][ty] < 0; d++) {
            boolean noway = true;
            for (int x = 0; x < grid.getXLength(); x++) {
                for (int y = 0; y < grid.getYLength(); y++) {
                    if (marks[x][y] == d) {
                        grid.setState(x, y, State.Active);
                        sleep();
                        int m = d + 1;
                        int top = y + 1, right = x + 1, bottom = y - 1, left = x - 1;
                        if (top < grid.getYLength() && marks[x][top] < 0 && !walls[x][top]) {
                            marks[x][top] = m;
                            grid.set(x, top, m, State.Yellow);
                            noway = false;
                        }
                        if (right < grid.getXLength() && marks[right][y] < 0 && !walls[right][y]) {
                            marks[right][y] = m;
                            grid.set(right, y, m, State.Yellow);
                            noway = false;
                        }
                        if (bottom >= 0 && marks[x][bottom] < 0 && !walls[x][bottom]) {
                            marks[x][bottom] = m;
                            grid.set(x, bottom, m, State.Yellow);
                            noway = false;
                        }
                        if (left >= 0 && marks[left][y] < 0 && !walls[left][y]) {
                            marks[left][y] = m;
                            grid.set(left, y, m, State.Yellow);
                            noway = false;
                        }
                        grid.setState(x, y, State.Yellow);
                        grid.setState(sx, sy, State.Stored);
                        grid.setState(tx, ty, State.Blue);
                        sleep();
                    }
                }
            }
            if (noway) {
                System.err.println("Path not found!");
                return;
            }
        }
        // Backtracing path
        int cx = tx, cy = ty;
        while (true) {
            grid.setState(cx, cy, State.Active);
            sleep();
            int m = marks[cx][cy] - 1;
            int top = cy + 1, right = cx + 1, bottom = cy - 1, left = cx - 1;
            grid.setState(cx, cy, State.Green);
            grid.setState(sx, sy, State.Stored);
            grid.setState(tx, ty, State.Blue);
            if (top < grid.getYLength() && marks[cx][top] == m) {
                cy = top;
                continue;
            }
            if (right < grid.getXLength() && marks[right][cy] == m) {
                cx = right;
                continue;
            }
            if (bottom >= 0 && marks[cx][bottom] == m) {
                cy = bottom;
                continue;
            }
            if (left >= 0 && marks[left][cy] == m) {
                cx = left;
                continue;
            }
            break;
        }
    }

}
