package jamav;

import javafx.scene.control.TextField;

/**
 *
 * @author pavelpopov
 */
public class NumberField extends TextField {

    public NumberField() {
        super("0");
    }

    public NumberField(int n) {
        super(String.valueOf(n));
    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (text.isEmpty() && start == 0 && end == getText().length()) {
            super.replaceText(0, getText().length(), "0");
        } else if (start < 3 && end - start <= 3 && text.matches("[0-9]+")) {
            super.replaceText(start, end, text);
        }
    }

    public int getNumber() {
        return Integer.valueOf(getText());
    }
}
