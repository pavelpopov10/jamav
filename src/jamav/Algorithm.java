package jamav;

/**
 *
 * @author pavelpopov
 */
public interface Algorithm {

    public void run(DisplayGrid grid);
}
